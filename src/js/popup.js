function init() {
    var data = loadJson("builds", {});

    var table = jQuery("#failedBuilds");
    sortedKeys(data).forEach(function (projectName) {
        var firstProject = true;

        //noinspection JSUnfilteredForInLoop
        var buildConfiguration = data[projectName];
        sortedKeys(buildConfiguration).forEach(function (buildConfigurationName) {
            var firstConfig = true;

            //noinspection JSUnfilteredForInLoop
            var projectData = buildConfiguration[buildConfigurationName];
            for (var branchName in projectData) {
                if (!projectData.hasOwnProperty(branchName)) continue;
                var x = projectData[branchName];

                if (firstProject) {
                    table.append($("<tr>").addClass("project").addClass("collapsed").attr("projectId", x.projectId)
                         .append($("<td>", {class: "image pr_" + x.projectId})
                         .append($("<img>").click(recalculate(x.projectId)).addClass("ctrl")))
                         .append($("<td>", {colspan: 6, class: "project"})
                         .append($("<span>")
                             .text(projectName)
                             .click(openLink(localStorage['server'], "project.html?projectId=%s", x.projectId)))));
                    firstProject = false;
                }

                if (firstConfig) {
                    var config = $("<tr>").addClass("conf")
                                          .addClass("collapsed")
                                          .attr("projectId", x.projectId)
                                          .attr("buildTypeId", x.buildTypeId)
                            .append($("<td>"))
                            .append($("<td>").addClass("image").attr("buildTypeId", x.buildTypeId)
                            .append($("<img>", {
                                src : "img/successfulExpanded.png",
                                click: recalculate(x.projectId, x.buildTypeId)}).addClass("ctrl")))
                            .append($("<td>", {colspan: 6, class: "conf"})
                            .append($("<span>", {
                                text: buildConfigurationName,
                                click: openLink(x.buildTypeUrl)})));
                    table.append(config);
                    firstConfig = false;
                }

                var row = $("<tr>").addClass("build")
                                   .addClass("collapsed")
                                   .attr("projectId", x.projectId)
                                   .attr("buildTypeId", x.buildTypeId)
                                   .addClass(x.isFailed ? "failed" : "succeeded")
                        .append($("<td>"))
                        .append($("<td>"))
                        .append($("<td>"))
                        .append($("<td>", {class: x.isDefaultBranch ? "default" : "branch"})
                        .append($("<span>", {
                            class: "link",
                            click: openLink(x.buildTypeUrl, "&branch_%s=%s", x.projectId, x.branchName),
                            text: x.branchName})))
                        .append($("<td>", {text: '#' + x.buildNumber}))
                        .append($("<td>", {class: "image"})
                        .append($("<img>").addClass("state")))
                        .append($("<td>")
                        .append($("<span>", {
                            class:"link",
                            click: openTab(x.buildUrl), text: trim(x.buildStatus)})));
                table.append(row);
            }
        });
    });

    // rebuild whole tree
    recalculate();

    if (localStorage['server']) {
        $("#server").children().remove();
        $("#server").append($("<span>", {click : openTab(localStorage['server'])}).addClass("link").text("TeamCity"));
    }
}

function sortedKeys(obj) {
    var arr = Object.keys(obj);
    arr.sort();
    return arr.filter(function (e) {return obj.hasOwnProperty(e)})
}

function recalculate(projectId, buildTypeId, project) {
    function update(prop, prefix) {
        return function () {
            var that = $(this);

            var data = loadJson('expanded', {});
            var key = prefix + ":" + projectId + ":" + buildTypeId;
            that.toggleClass(prop);
            if (that.hasClass(prop)) {
                that.attr("src", that.hasClass("failed") ? "img/failedCollapsed.png" : "img/successfulCollapsed.png");
                delete data[key];
            } else {
                that.attr("src", that.hasClass("failed") ? "img/failedExpanded.png" : "img/successfulExpanded.png");
                data[key] = true;
            }

            saveJson('expanded', data);
        }
    }

    function get(c, projectId, buildTypeId) {
        if (buildTypeId) {
            return $('tr.' + c + '[projectId="' + projectId + '"][buildTypeId="' + buildTypeId + '"]')
        } else {
            return $('tr.' + c + '[projectId="' + projectId + '"]')
        }
    }

    if (!arguments.length) {
        // propagate failed builds state to project & conf
        var failedBuilds = $("tr.build.failed");
        failedBuilds.each(function () {
            var buildThis = $(this);
            var buildTypeId = buildThis.attr("buildTypeId");
            var projectId = buildThis.attr("projectId");
            get('conf', projectId, buildTypeId).addClass("failed").removeClass("collapsed");
            get('project', projectId).addClass("failed").removeClass("collapsed");
            buildThis.removeClass("collapsed");
        });

        function sync() {
            $("tr").each(function() {
                var that = $(this);
                var controls = that.find("img.ctrl");
                if (that.hasClass("collapsed")) {
                    controls.addClass("collapsed")
                } else {
                    controls.removeClass("collapsed")
                }

                if (that.hasClass("failed")) {
                    controls.addClass("failed")
                } else {
                    controls.removeClass("failed")
                }
            }).find("img.ctrl").attr("src", "img/successfulExpanded.png");

            failedBuilds.find("img.state").attr("src", "img/buildFailed.gif");
            var configs = $("tr.conf");
            configs.find("img.ctrl").attr("src", "img/successfulExpanded.png");
            $("tr.collapsed").hide().find("img.ctrl").attr("src", "img/successfulCollapsed.png");
            $("tr.failed").find("img.ctrl").attr("src", "img/failedExpanded.png");
            $("tr.failed.collapsed").find("img.ctrl").attr("src", "img/failedCollapsed.png");
            $("tr.build").not(".failed").find("img.state").attr("src", "img/buildSuccessful.jpg");

            $("tr.project").each(function() {
                var projectThis = $(this);
                var projectId = projectThis.attr("projectId");
                projectThis.show();
                if (!projectThis.hasClass("collapsed")) {
                    get('conf', projectId).show().each(function() {
                        var confThis = $(this);
                        var buildTypeId = confThis.attr("buildTypeId");
                        if (!confThis.hasClass("collapsed")) {
                            get('build', projectId, buildTypeId).show();
                        }
                    });
                }
            });
        }

        sync();
        sortedKeys(loadJson('expanded', {})).forEach(function (key) {
            var x = key.split(":");
            var projectId = x[1];
            var buildTypeId = x[2];
            if (x[0] === "project") {
                get('project', projectId).removeClass("collapsed");
            } else {
                get('conf', projectId, buildTypeId).removeClass("collapsed")
            }
            sync();
        });

        return function() {};
    }

    return function() {
        if (!buildTypeId) {
            var project = get('project', projectId).first();
            project.toggleClass("collapsed").find("img.ctrl").each(update("collapsed", "project"));
            get('conf', projectId).toggle().each(function() {
                var that = $(this);
                var buildTypeId = that.attr("buildTypeId");
                var builds = get('build', projectId, buildTypeId);
                if (that.hasClass("collapsed")) {
                    builds.hide()
                } else {
                    builds.show();
                }
            });
            if (project.hasClass("collapsed")) {
                get('build', projectId, buildTypeId).hide();
            }
        } else {
            get('conf', projectId, buildTypeId)
                    .toggleClass("collapsed")
                    .find("img.ctrl")
                    .each(update("collapsed", "conf"));
            get('build', projectId, buildTypeId)
                    .toggleClass("collapsed")
                    .toggle();
        }
    }
}

$(window).load(function() {
    $("body").fadeOut(10, function() { init(); $(this).show(); });
});
