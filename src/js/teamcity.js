function init() {
    var server = localStorage["server"];

    if (server) {
        console.log("server loaded:", server);
        if (server[server.length - 1] != "/") {
            server = server + "/";
        }
        statusChecker(server);
    } else {
        setBadge("warning", "Setup");
        chrome.tabs.create({
            url: chrome.extension.getURL('options.html')
        });
        console.log("error: no server");
    }
}

function setBadge(notification, text) {
    var color = [0, 255, 0, 255];
    if (notification == "warning") {
        color = [255, 0, 255, 255];
    } else if (notification == "error") {
        color = [255, 0, 0, 255];
    } else if (notification == "notice") {
        color = [0, 0, 255, 255];
    }
    chrome.browserAction.setBadgeBackgroundColor({color:color});
    if (text) {
        chrome.browserAction.setBadgeText({text:text});
    }
}

function statusChecker(server) {
    function drawIconAtRotation() {
        function ease(x) {
            return (1 - Math.sin(Math.PI / 2 + x * Math.PI)) / 2;
        }

        canvasContext.save();
        canvasContext.clearRect(0, 0, canvas.width, canvas.height);
        canvasContext.translate(Math.ceil(canvas.width / 2), Math.ceil(canvas.height / 2));
        canvasContext.rotate(2 * Math.PI * ease(rotation));
        canvasContext.drawImage(icon, -Math.ceil(canvas.width / 2), -Math.ceil(canvas.height / 2));
        canvasContext.restore();

        chrome.browserAction.setIcon({
                imageData: canvasContext.getImageData(0, 0, canvas.width, canvas.height)
        });
    }

    function animateFlip() {
        var animationSpeed = 10;
        var animationFrames = 36;
        rotation = rotation + 1 / animationFrames;
        drawIconAtRotation();

        if (rotation <= 1) {
            setTimeout(animateFlip, animationSpeed);
        } else {
            rotation = 0;
            drawIconAtRotation();
        }
    }

    function checkNewBuilds(statuses) {
        var obj = loadJson("builds", {});
        var toDelete = loadJson("buildTypes", []);
        function scanAndDelete(projectId, buildTypeId) {
            var copy = toDelete.slice(0);
            for (var i=0; i < toDelete.size; i++) {
                if (obj.id == buildTypeId || obj.projectId == projectId) {
                    copy.splice(i, 1);
                }
            }

            toDelete = copy;
        }

        for (var key in statuses) {
            if (!statuses.hasOwnProperty(key)) {
                continue;
            }

            var x = key.split("::");
            var buildType = x[0];
            var branchName = x[1];
            var buildId = x[2];
            var defaultBranch = branchName == "default";

            request(toLink(server, "app/rest/builds/buildType:(id:%s),branch:(%s),id:%s",
                           buildType, defaultBranch ? "default:true" : "name:" + branchName, buildId),
                    function(responseXML) {
                        var details = build(responseXML);
                        if (!obj[details.projectName]) {
                            obj[details.projectName] = {}
                        }

                        if (!obj[details.projectName][details.buildName]) {
                            obj[details.projectName][details.buildName] = {}
                        }

                        details.isDefaultBranch = defaultBranch;
                        details.buildTypeId = buildType;
                        obj[details.projectName][details.buildName][details.branchName] = details;

                        scanAndDelete(details.projectId, details.buildTypeId);
                    },
                    function () {
                        setBadge("warning", "?")
                    });
        }

        for (var i=0; i < toDelete.size; i++) {
            // TODO: remove data from obj
        }

        console.log("  builds", obj);
        saveJson("builds", obj);
    }

    function build(responseXML) {
        function getVal(name) {
            return responseXML.getElementsByTagName(name)[0].firstChild.nodeValue
        }

        function getAttr(elName, attrName) {
            var attribute = responseXML.getElementsByTagName(elName)[0].attributes[attrName];
            if (attribute) {
                return attribute.value;
            }
            return null;
        }

        var status = getAttr("build", "status");
        var branchName = getAttr("build", "branchName");
        return {
            buildTypeUrl: getAttr("buildType", "webUrl"),
            buildTypeId: getAttr("build", "buildTypeId"),
            projectId: getAttr("buildType", "projectId"),
            buildNumber: getAttr("build", "number"),
            buildName: getAttr("buildType", "name"),
            buildUrl:  getAttr("build", "webUrl"),
            projectName: getAttr("buildType", "projectName"),
            projectUrl: getAttr("build", "webUrl"),
            isDefaultBranch: getAttr("build", "defaultBranch") != null,
            branchName: branchName == null ? "default" : branchName,
            buildStatus: getVal("statusText"),
            isFailed: status == "FAILURE" || status == "ERROR"
        }
    }

    function success(responseXML) {
        var statuses = responseXML.getElementsByTagName("build");

        var count = loadJson("errorCount", {});
        var buildStatus = {};

        console.log("  response", responseXML);
        for (var i = 0; i < statuses.length; i++) {
            var a = statuses[i].attributes;
            var id = a["buildTypeId"].value;
            var branchElement = a["branchName"];

            if (i == 0) {
                count[id] = 0;
            }

            var status = a["status"].value;
            // we can't thread such build (fixes #1)
            if (status == "UNKNOWN") {
                continue;
            }

            var branch = "default";
            if (branchElement && !a["defaultBranch"] && branchElement.value != null) {
                branch = branchElement.value;
            }

            var found = false;
            for (var st in buildStatus) {
                if (!buildStatus.hasOwnProperty(st)) continue;
                if (st.startsWith(id + "::" + branch)) {
                    found = true;
                    break
                }
            }

            if (!found) {
                buildStatus[id + "::" + branch + "::" + a['id'].value] = status;
                if (status == "FAILURE" || status == "ERROR") {
                    count[id]++;
                }
            }
        }

        console.log("  statuses", buildStatus);

        var result = 0;
        for (var bId in count) {
            if (!count.hasOwnProperty(bId)) continue;
            result += count[bId];
        }

        saveJson("errorCount", count);

        if (result == 0) {
            setBadge("success", "OK");
        } else {
            setBadge("error", result.toString());
        }

        checkNewBuilds(buildStatus);
        requestFailureCount = 0;
    }

    var rotation = 0;
    var canvas = document.getElementById('canvas');
    var icon = document.getElementById('icon');
    var canvasContext = canvas.getContext("2d");
    animateFlip();

    function fetchBuilds(responseXML) {
        var types = responseXML.getElementsByTagName("buildType");
        console.log("  types", types);

        var ids = [];
        for (var i = 0; i < types.length; i++) {
            var a = types[i].attributes;
            var id = a["id"].value;
            var projectId = a["projectId"].value;
            ids.push({
                id: id,
                projectId: projectId
            });

            request(server + "app/rest/builds?locator=branch:(branched:any),buildType:" + id,
                    success, function () {setBadge("warning", "?")});
        }

        saveJson("buildTypes", ids);
    }

    ajaxRequest(server + "app/rest/buildTypes",
            fetchBuilds, function () {
                setBadge("warning", "?");
            }, true);
}

function request(url, onSuccess, onError) {
    console.log("Synchronous request request:", url);
    try {
        var req = new XMLHttpRequest();
        req.open("GET", url, false);
        req.send(null);
        onSuccess(req.responseXML)
    } catch (e) {
        onError();
    }
}

function ajaxRequest(url, onSuccess, onError, again) {
    function scheduleRequest() {
        var pollIntervalMin = 1000 * 60;  // 1 minute
        var pollIntervalMax = 1000 * 60 * 5;  // 5 minutes

        var randomness = Math.random() * 2;
        var exponent = Math.pow(2, requestFailureCount);
        var delay = Math.min(randomness * pollIntervalMin * exponent, pollIntervalMax);
        delay = Math.round(delay);

        console.log("Scheduling request:", delay, "ms");
        window.setTimeout(startRequest, delay);
    }

    function startRequest() {
        var requestTimeout = 1000 * 10;  // 10 seconds
        console.log("Starting request");

        abortTimeoutId = window.setTimeout(function () {
            console.log("aborting!");
            req.abort();
        }, requestTimeout);
        console.log("  abort timer set:", requestTimeout, "ms");

        req.open("GET", url, true);
        req.send(null);

        console.log("  GET request sent:", url);
    }

    function handleStateChange() {
        try {
            console.log("  state changed", this.readyState, this.status);
            if (this.readyState == 4) { // finished
                if (this.status == 200 && this.responseXML != null) { // success
                    console.log("  success!");
                    if (onSuccess) {
                        onSuccess(this.responseXML);
                    }
                } else { // failure
                    console.log("  error (status: " + this.status + ", response: " + this.responseXML + ")");
                    requestFailureCount++;
                    if (onError) {
                        onError();
                    }
                }

                if (abortTimeoutId) {
                    console.log("  abort timer cleared");
                    window.clearTimeout(abortTimeoutId);
                }

                if (again) {
                    scheduleRequest();
                }
            }
        } catch (e) {
        }
    }

    var requestFailureCount = 0;  // used for exponential backoff
    var abortTimeoutId;
    var req = new XMLHttpRequest();
    req.onreadystatechange = handleStateChange;
    startRequest();
}

window.addEventListener('load', init);
